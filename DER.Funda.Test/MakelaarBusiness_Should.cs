﻿using DER.Funda.Core;
using DER.Funda.Core.Domain.Entities;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace DER.Funda.Test
{
    public class MakelaarBusiness_Should
    {
        private Mock<IMakelaarBusiness> _makelaarBusinessMoq;

        public MakelaarBusiness_Should()
        {
            _makelaarBusinessMoq = new Mock<IMakelaarBusiness>();
            SetUpMoqBussiness();
        }

        [Fact]
        public void MakelaarBusiness_RetrieveAmount_ReturnTrue()
        {
            var result = _makelaarBusinessMoq.Object.GetObjectsByQueryAsync("Amsterdam").Result;
            Assert.Equal(10, result.Count);
        }

        [Fact]
        public void MakelaarBusiness_RetrieveOrderedByAmount_ReturnTrue()
        {
            var result = _makelaarBusinessMoq.Object.GetObjectsByQueryOrderedAsync("Amsterdam", x => x.ObjectsAmount).Result;
            Assert.True(result.FirstOrDefault().ObjectsAmount == 10);
        }

        [Fact]
        public void MakelaarBusiness_RetrieveOrderedByAmount_Returnfalse()
        {
            var result = _makelaarBusinessMoq.Object.GetObjectsByQueryOrderedAsync("Amsterdam", x => x.ObjectsAmount).Result;
            Assert.False(result.FirstOrDefault().ObjectsAmount == 1);
        }

        [Fact]
        public void MakelaarBusiness_RetrieveOrderedByAmountWithTop_Returntrue()
        {
            var result = _makelaarBusinessMoq.Object.GetTopObjectsByQueryOrderedAsync("Amsterdam", x => x.ObjectsAmount, 5).Result;
            Assert.True(result.Count == 5);
        }

        [Fact]
        public void MakelaarBusiness_RetrieveOrderedByAmountWithTop_Returnfalse()
        {
            var result = _makelaarBusinessMoq.Object.GetTopObjectsByQueryOrderedAsync("Amsterdam", x => x.ObjectsAmount, 5).Result;
            Assert.False(result.Count == 10);
        }

        private void SetUpMoqBussiness()
        {
            List<Makelaar> makelaars = GenerateMakelaarData();
            _makelaarBusinessMoq.Setup(x => x.GetObjectsByQueryAsync("Amsterdam")).Returns(Task.FromResult(makelaars.ToList()));
            _makelaarBusinessMoq.Setup(x => x.GetObjectsByQueryOrderedAsync("Amsterdam", It.IsAny<Func<Makelaar, int>>())).Returns(Task.FromResult(makelaars.OrderByDescending(x => x.ObjectsAmount).ToList()));
            _makelaarBusinessMoq.Setup(x => x.GetTopObjectsByQueryOrderedAsync("Amsterdam", It.IsAny<Func<Makelaar, int>>(), 5)).Returns(Task.FromResult(makelaars.OrderByDescending(x => x.ObjectsAmount).Take(5).ToList()));
        }

        private static List<Makelaar> GenerateMakelaarData()
        {
            return new List<Makelaar>() {
            new Makelaar() { MakelaarID = 1, Name = "Makelaar_1", ObjectsAmount = 1 },
            new Makelaar() { MakelaarID = 2, Name = "Makelaar_2", ObjectsAmount = 2 },
            new Makelaar() { MakelaarID = 3, Name = "Makelaar_3", ObjectsAmount = 3 },
            new Makelaar() { MakelaarID = 4, Name = "Makelaar_4", ObjectsAmount = 4 },
            new Makelaar() { MakelaarID = 5, Name = "Makelaar_5", ObjectsAmount = 5 },
            new Makelaar() { MakelaarID = 6, Name = "Makelaar_6", ObjectsAmount = 6 },
            new Makelaar() { MakelaarID = 7, Name = "Makelaar_7", ObjectsAmount = 7 },
            new Makelaar() { MakelaarID = 8, Name = "Makelaar_8", ObjectsAmount = 8 },
            new Makelaar() { MakelaarID = 9, Name = "Makelaar_9", ObjectsAmount = 9 },
            new Makelaar() { MakelaarID = 10, Name = "Makelaar_10", ObjectsAmount = 10 }
            };
        }
    }
}
