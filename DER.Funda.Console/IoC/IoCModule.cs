﻿using DER.Funda.Core;
using DER.Funda.Core.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DER.Funda.Console.IoC
{
    public static class IoCModule
    {
        public static void BindBusiness(this IServiceCollection services) => services.AddTransient<IMakelaarBusiness, MakelaarBusiness>();

        public static void BindManager(this IServiceCollection services) => services.AddSingleton<IWorkerManager, WorkerManager>();

        public static void BindRestClient(this IServiceCollection services)
        {
            services.AddSingleton<IFundaClient>(factory =>
            {
                var conf = new FundaRestClientConfiguration();
                factory.GetService<IConfiguration>().GetSection("FundaApi").Bind(conf);
                return new FundaClient(conf.Secret, conf.URL, conf.DataType);
            });
        }

    }
}
