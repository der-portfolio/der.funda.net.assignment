﻿using System.Threading;
using System.Threading.Tasks;

namespace DER.Funda.Console
{
    public interface IWorkerManager
    {
        Task Start(CancellationToken stoppingToken);
    }
}
