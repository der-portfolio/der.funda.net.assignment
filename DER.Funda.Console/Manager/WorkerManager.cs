﻿using System;
using System.Threading;
using System.Threading.Tasks;
using ConsoleTableExt;
using DER.Funda.Core;
using DER.Funda.UI;

namespace DER.Funda.Console
{
    public class WorkerManager : IWorkerManager
    {
        private IMakelaarBusiness _bus;

        public WorkerManager(IMakelaarBusiness bus)
        {
            _bus = bus;
        }

        public async Task Start(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await RunEngine();
                System.Console.ReadKey();
            }
        }

        private Task RunEngine()
        {
            CLS();
            var option = AppMenu.PrintMenu();

            switch (option)
            {
                case "1":
                    return QryAndPrint("/amsterdam");
                case "2":
                    return QryAndPrint("/amsterdam/tuin");
                case "3":
                    Environment.FailFast("Panic Exit");
                    break;
                default:
                    System.Console.WriteLine("Wrong option!");
                    break;
            }

            return Task.CompletedTask;
        }

        private async Task QryAndPrint(string qry)
        {
            CLS();
            var dataTable = await _bus.GetTopObjectsByQueryOrderedAsync(qry, x => x.ObjectsAmount, 10);
            ConsoleTableBuilder.From(dataTable)
                               .WithFormat(ConsoleTableBuilderFormat.Minimal)
                               .ExportAndWriteLine();
            System.Console.WriteLine("Please press any key to continue...");
            return;
        }

        private void CLS() => System.Console.Clear();
    }
}
