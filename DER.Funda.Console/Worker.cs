using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace DER.Funda.Console
{
    public class Worker : BackgroundService
    {
        private readonly IWorkerManager _manager;

        public Worker(ILogger<Worker> logger, IWorkerManager manager)
        {
            _manager = manager;
        }

        public override Task StartAsync(CancellationToken stoppingToken) => _manager.Start(stoppingToken);

        public override Task StopAsync(CancellationToken stoppingToken) => Task.CompletedTask;

        protected override Task ExecuteAsync(CancellationToken stoppingToken) => Task.CompletedTask;
    }
}
