﻿using System;
using System.Drawing;

namespace DER.Funda.UI
{
    public class AppMenu
    {
        static Action<string> PrintLine = f => Colorful.Console.WriteLine(f);
        static Action<string> Print = f => Colorful.Console.Write(f);

        public static string PrintMenu()
        {
            InitArt();
            PrintLine("\n\n\n");
            PrintLine("Please choose an option and press enter: \n");
            PrintLine("1 - Check top 10 markelaar stadistics in Amsterdam \n");
            PrintLine("2 - Check top 10 Markelaar stadistics filtered by garden in Amsterdam \n");
            PrintLine("3 - Exit \n");
            Print("Option : ");

            return System.Console.ReadLine();
        }

        private static void InitArt()
        {
            Colorful.Console.WriteWithGradient(@"
                                         +              #####
                                        / \
      _____        _____     __________/ o \/\_________      _________         ╔═╗┬ ┬┌┐┌┌┬┐┌─┐  ╔═╗┬  ┬┌─┐┌┐┌┌┬┐   
     |o o o|_______|    |___|               | | # # #  |____|o o o o  | /\     ╠╣ │ ││││ ││├─┤  ║  │  │├┤ │││ │   
     |o o o|  * * *|: ::|. .|               |o| # # #  |. . |o o o o  |//\\    ╚  └─┘┘└┘─┴┘┴ ┴  ╚═╝┴─┘┴└─┘┘└┘ ┴       
     |o o o|* * *  |::  |. .| []  []  []  []|o| # # #  |. . |o o o o  |((|))                         ┌┐ ┬ ┬  ╔╦╗┌─┐┬─┐
     |o o o|**  ** |:  :|. .| []  []  []    |o| # # #  |. . |o o o o  |((|))                         ├┴┐└┬┘   ║║├┤ ├┬┘
     |_[]__|__[]___|_||_|__<|____________;;_|_|___/\___|_.|_|____[]___|  |                           └─┘ ┴   ═╩╝└─┘┴└─ ", Color.Yellow, Color.Fuchsia, 14);
        }
    }
}
