<p align="center">
  <h3 align="center">DER.Funda.NET.Assignment</h3>

  <p align="center">
    Funda API Client Assignment Solution
    <br />
    <a href="https://gitlab.com/der-portfolio/der.funda.net.assignment/"><strong>Explore the repository »</strong></a>
    <br />
    <br />
    ·
    <a href="https://gitlab.com/der-portfolio/der.funda.net.assignment/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/der-portfolio/der.funda.net.assignment/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->
## About The Project

.Net Implementation for Funda Api Rest Client Assignment, the client implement a throttling mecanism using a semaphore in order to be able of not getting blocked because of the API request limits (> 100 request per minute) . It uses async methods to resolve the records retrieve and analysis. It was build using SOLID principles.


### Built With

* [.NetCore](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-3.1)
* [Colorful.Console](http://colorfulconsole.com/)
* [ConsoleTableExt](https://github.com/minhhungit/ConsoleTableExt/)
* [Luna.ProgressBar](https://github.com/refactorsaurusrex/console-progress-bar)
* [XUnit](https://github.com/xunit/xunit)



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

You will need Dotnet Cli for restoring commands, https://docs.microsoft.com/en-us/dotnet/core/sdk

### Installation

1. Clone the repo
```sh
git clone https://gitlab.com/der-portfolio/der.funda.net.assignment/
```
2. CD .\der.funda.net.assignment

3. dotnet restore

4. dotnet build (optional --configuration release)

5. cd .\DER.Funda.Console\bin\Debug\netcoreapp3.1  (or insted of debug folder, release folder, depends on your step 4 configuration)

6. run Der.Funda.Console.exe


<!-- USAGE EXAMPLES -->
## Usage

Choose option 1 or 2 and wait until the progress bar reach 100%

