﻿namespace DER.Funda.Core.Configuration
{
    public class FundaRestClientConfiguration
    {
        public string Secret { get; set; }
        public string URL { get; set; }
        public string DataType { get; set; }
    }
}
