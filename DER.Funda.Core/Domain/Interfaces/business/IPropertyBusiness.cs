﻿using DER.Funda.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DER.Funda.Core
{
    public interface IMakelaarBusiness
    {
        /// <summary>
        /// Retrieve Makelaar objects
        /// </summary>
        /// <param name="zo">Qry</param>
        /// <returns></returns>
        Task<List<Makelaar>> GetObjectsByQueryAsync(string zo);
        /// <summary>
        /// Retrieve Ordered
        /// </summary>
        /// <param name="zo">Qry</param>
        /// <param name="orderBy">Key Selector</param>
        /// <returns></returns>
        Task<List<Makelaar>> GetObjectsByQueryOrderedAsync(string zo, Func<Makelaar, int> orderBy);
        /// <summary>
        /// Retrieve Top X Ordered
        /// </summary>
        /// <param name="zo">Qry</param>
        /// <param name="orderBy">Key Selector</param>
        /// <param name="top">Top amount</param>
        /// <returns></returns>
        Task<List<Makelaar>> GetTopObjectsByQueryOrderedAsync(string zo, Func<Makelaar, int> orderBy, int top);

    }
}
