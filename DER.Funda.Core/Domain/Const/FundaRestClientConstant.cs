﻿namespace DER.Funda.Core.Domain.Const
{
    public class FundaRestClientConstant
    {
        public const int ParalellRequestAmount = 10;
        public const int LimitingPeriodInMilliSeconds = 6;
        public const int MaxPagingSize = 25;
    }
}
