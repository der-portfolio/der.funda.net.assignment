﻿namespace DER.Funda.Core.Domain.Entities
{
    public class Makelaar
    {
        public long MakelaarID { get; set; }
        public string Name { get; set; }
        public int ObjectsAmount { get; set; }
    }
}
