﻿using Der.Funda.Dtos;
using DER.Funda.Core.Domain.Const;
using DER.Funda.Core.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DER.Funda.Core
{
    public class MakelaarBusiness : IMakelaarBusiness
    {
        private IFundaClient _client;
        private int _requestLimit;
        private int _limitingPeriodInSeconds;
        private SemaphoreSlim _throttler;

        public MakelaarBusiness(IFundaClient client)
        {
            _client = client;
            _requestLimit = FundaRestClientConstant.ParalellRequestAmount;
            _limitingPeriodInSeconds = FundaRestClientConstant.LimitingPeriodInMilliSeconds;
            _throttler = new SemaphoreSlim(_requestLimit);
        }

        #region I.Public
        public async Task<List<Makelaar>> GetObjectsByQueryAsync(string zo)
        {
            try
            {
                var makelaarResultList = await FirstCall(zo);
                return makelaarResultList.ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Error: {ex}");
            }
        }

        public async Task<List<Makelaar>> GetObjectsByQueryOrderedAsync(string zo, Func<Makelaar, int> orderBy)
        {
            try
            {
                var makelaarResultList = await GetObjectsByQueryAsync(zo);
                return OrderByDescending(makelaarResultList, orderBy).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Error: {ex}");
            }
        }

        public async Task<List<Makelaar>> GetTopObjectsByQueryOrderedAsync(string zo, Func<Makelaar, int> orderBy, int top)
        {
            try
            {
                var makelaarResultList = await GetObjectsByQueryAsync(zo);
                return OrderByDescending(makelaarResultList, orderBy, top).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception($"Error: {ex}");
            }
        }

        #endregion

        #region II. Private

        private async Task<List<Makelaar>> FirstCall(string zo)
        {
            var objectDtoList = new List<Der.Funda.Dtos.Object>();
            var makelaarResultList = new List<Makelaar>();
            var firstPageObjects = await Handshake(zo);

            await PullAndPopulate(zo, objectDtoList, firstPageObjects);
            GenerateList(objectDtoList, makelaarResultList);
            return makelaarResultList;
        }

        private async Task<FundaApiResponse> Handshake(string zo) => await _client.GetObjectsByQueryAsync(zo);

        private async Task PullAndPopulate(string zo, List<Der.Funda.Dtos.Object> objectList, FundaApiResponse firstPageObjects)
        {
            //DER: Need to create the progress bar here because of a problem 
            //with the synchronization context and the console, but I prefer to manage this from UI layer

            var progress = ProgressDisplayFacility.GenerateProgressBar();
            Console.Clear();
            Console.Write($"Downloading pages :");
            using ((IDisposable)progress)
            {
                for (var pag = 2; pag <= firstPageObjects.Paging.AantalPaginas; pag++)
                {
                    await _throttler.WaitAsync();
                    var webCall = _client.GetObjectsByQueryAsync(zo, OperationTypeConstant.koop, pag, FundaRestClientConstant.MaxPagingSize);
                    progress.Report((double)pag / firstPageObjects.Paging.AantalPaginas);

                    _ = webCall.ContinueWith(completed =>
                    {
                        switch (completed.Status)
                        {
                            case TaskStatus.RanToCompletion:
                                objectList.AddRange(completed.Result.Objects);
                                break;
                            case TaskStatus.Faulted:
                                Console.WriteLine(completed.Exception);
                                break;
                        }
                    }, TaskScheduler.Default);

                    _ = webCall.ContinueWith(async s =>
                    {
                        await Task.Delay(100 * _limitingPeriodInSeconds);
                        _throttler.Release();
                    });
                    await webCall;
                }
                progress.Report(1);
            }
            Console.WriteLine("");

        }

        private static void AddOrUpdate(List<Makelaar> makelaars, Der.Funda.Dtos.Object dtoObjects)
        {
            if (makelaars.Any(x => x.MakelaarID == dtoObjects.MakelaarId))
                makelaars.Find(j => j.MakelaarID == dtoObjects.MakelaarId).ObjectsAmount++;
            else
                makelaars.Add(new Makelaar() { MakelaarID = dtoObjects.MakelaarId, Name = dtoObjects.MakelaarNaam, ObjectsAmount = 1 });
        }

        private void GenerateList(List<Der.Funda.Dtos.Object> objectList, List<Makelaar> makelaars) => objectList.ForEach(f => AddOrUpdate(makelaars, f));

        private IEnumerable<Makelaar> OrderByDescending(List<Makelaar> makelaars, Func<Makelaar, int> keySelector, int top = 10) => makelaars.OrderByDescending(keySelector).Take(top);

        #endregion
    }
}
