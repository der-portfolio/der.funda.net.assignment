﻿using Der.Funda.Dtos;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace DER.Funda.Core
{
    public class FundaClient : IFundaClient
    {
        private HttpClient _client;

        public FundaClient(string apiKey, string url, string dataType)
        {
            _client = new HttpClient()
            {
                BaseAddress = new Uri($"{url}/{dataType}/{apiKey}/")
            };
        }

        public async Task<FundaApiResponse> GetObjectsByQueryAsync(string zo, string type = "koop", int pageNumber = 1, int pageSize = 25)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(zo))
                    zo = string.Concat("&zo=", zo);

                var responseBody = await GetAsync($"?type={type}{zo}/&page={pageNumber}&pagesize={pageSize}");
                var responseContent = await responseBody.Content.ReadAsStreamAsync();

                return FundaApiDeserializer.FromJsonStreamAsync(responseContent);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                throw new Exception(e.ToString());
            }
        }

        private async Task<HttpResponseMessage> GetAsync(string path)
        {
            HttpResponseMessage response = await _client.GetAsync(path);
            response.EnsureSuccessStatusCode();
            return response;
        }
    }
}
