﻿using Der.Funda.Dtos;
using System.Threading.Tasks;

namespace DER.Funda.Core
{
    public interface IFundaClient
    {
        Task<FundaApiResponse> GetObjectsByQueryAsync(string zo, string type = "koop", int pageNumber = 1, int pageSize = 25);
    }
}
