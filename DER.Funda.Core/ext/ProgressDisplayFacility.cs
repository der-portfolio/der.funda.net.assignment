﻿using Luna.ConsoleProgressBar;
using System;

namespace DER.Funda.Core
{
    public class ProgressDisplayFacility
    {
        public static IProgress<double> GenerateProgressBar()
        {
            return new ConsoleProgressBar
            {
                NumberOfBlocks = 30,
                ForegroundColor = ConsoleColor.Green,
                StartBracket = string.Empty,
                EndBracket = string.Empty,
                CompletedBlock = "\u2022",
                IncompleteBlock = "·",
                AnimationSequence = UniversalProgressAnimations.RotatingTriangle
            };
        }
    }
}
