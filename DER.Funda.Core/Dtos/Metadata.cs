﻿using Newtonsoft.Json;

namespace Der.Funda.Dtos
{
    public partial class Metadata
    {
        [JsonProperty("ObjectType")]
        public string ObjectType { get; set; }

        [JsonProperty("Omschrijving")]
        public string Omschrijving { get; set; }

        [JsonProperty("Titel")]
        public string Titel { get; set; }
    }
}
