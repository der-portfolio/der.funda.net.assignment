﻿using Newtonsoft.Json;

namespace Der.Funda.Dtos
{
    public partial class Paging
    {
        [JsonProperty("AantalPaginas")]
        public long AantalPaginas { get; set; }

        [JsonProperty("HuidigePagina")]
        public long HuidigePagina { get; set; }

        [JsonProperty("VolgendeUrl")]
        public object VolgendeUrl { get; set; }

        [JsonProperty("VorigeUrl")]
        public object VorigeUrl { get; set; }
    }
}
