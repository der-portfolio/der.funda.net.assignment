﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Der.Funda.Dtos
{
    public partial class FundaApiResponse
    {
        [JsonProperty("AccountStatus")]
        public long AccountStatus { get; set; }

        [JsonProperty("EmailNotConfirmed")]
        public bool EmailNotConfirmed { get; set; }

        [JsonProperty("ValidationFailed")]
        public bool ValidationFailed { get; set; }

        [JsonProperty("ValidationReport")]
        public object ValidationReport { get; set; }

        [JsonProperty("Website")]
        public long Website { get; set; }

        [JsonProperty("Metadata")]
        public Metadata Metadata { get; set; }

        [JsonProperty("Objects")]
        public List<Object> Objects { get; set; }

        [JsonProperty("Paging")]
        public Paging Paging { get; set; }

        [JsonProperty("TotaalAantalObjecten")]
        public long TotaalAantalObjecten { get; set; }
    }
}
