﻿using Newtonsoft.Json;
using System.IO;

namespace Der.Funda.Dtos
{
    public partial class FundaApiDeserializer
    {
        public static FundaApiResponse FromJson(string json) => JsonConvert.DeserializeObject<FundaApiResponse>(json, Converter.Settings);

        public static FundaApiResponse FromJsonStreamAsync(Stream stream)
        {
            using (StreamReader sr = new StreamReader(stream))
            using (JsonReader reader = new JsonTextReader(sr))
            {
                JsonSerializer serializer = new JsonSerializer()
                {
                    MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                    DateParseHandling = DateParseHandling.None,
                    NullValueHandling = NullValueHandling.Ignore
                };

                return serializer.Deserialize<FundaApiResponse>(reader);
            }
        }
    }
}
