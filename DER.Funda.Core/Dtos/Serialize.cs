﻿using Newtonsoft.Json;

namespace Der.Funda.Dtos
{
    public static class Serialize
    {
        public static string ToJson(this FundaApiResponse self) => JsonConvert.SerializeObject(self, Der.Funda.Dtos.Converter.Settings);
    }
}
